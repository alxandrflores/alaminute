<?php
	require 'connect.php';

  function searchRecipe($name){
    echo "<div class='searchResults'><p class='searchResultsText'>Search Results for '". $name."'</p></div>";

    $search_match_query = "SELECT r_name, r_url, r_id FROM recipe WHERE r_name LIKE '%$name%' AND r_id NOT IN 
    (SELECT r_id FROM r_uses_i WHERE r_uses_i.i_id not IN 
    (SELECT i_id FROM mykitchen))";

    $search_additional_match_query = "SELECT r_name, r_url, r_id FROM recipe WHERE r_name LIKE '%$name%' AND r_id IN 
    (SELECT r_id FROM r_uses_i WHERE r_uses_i.i_id not IN 
    (SELECT i_id FROM mykitchen))";
    //echo "1";

    $search_match_query_result = mysql_query($search_match_query); //run query
    //echo "2";
    $search_additional_match_query_result = mysql_query($search_additional_match_query); //run query
    //echo "3";
      //If query invalid
      if(!$search_match_query_result) {
        $message = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' .$search_match_query;
        die($message);
      }

      //If query invalid
      if(!$search_additional_match_query_result) {
        $message = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' .$search_additional_match_query;
        die($message);
      }
      // Print names/images of meals that are matches
     // echo "4";
    while($recipe_names = mysql_fetch_assoc($search_match_query_result)){
        echo '<div class="reciperesult">';
    
        echo '<div class="recipeinfo"><li class="recipelink"><a href=" '. $recipe_names["r_url"] . ' " target="_blank" >' . $recipe_names["r_name"] . ' </a></li></div>';
  
        echo '<img src = "/alaminute/img/'.$recipe_names['r_name'].'.jpg" width="250" height="200">';
        echo '</div>';
      }
      // Print names/images of meals that are additional matches
      echo "<br>________________________________________________________________________<br>";
    
  // Display results of additional matches
    while($joined = mysql_fetch_assoc($search_additional_match_query_result)){
    
            echo '<div class="reciperesult">';
             echo '<div class="recipeinfo"><ul class="recipeHeader"><li class="recipelink"><a href=" '. $joined["r_url"] . ' " target="_blank" >' . $joined["r_name"] . ' </a></li><li class="infoButton"><input class="moreIngredientsView" type="button"/></li></ul></div>';
             $id_name_nospace = str_replace(' ', '', $joined['r_name']);
             $id_name_nospace = preg_replace('/\s+/', '', $id_name_nospace);
             echo '<img src = "/alaminute/img/'.$joined['r_name']. '.jpg" width="250" height="200">';
      
      // Get additional ingredients for each recipe 
        $get_ing = sprintf("SELECT i_name FROM ingredients, r_uses_i, recipe 
              WHERE r_uses_i.r_id=recipe.r_id AND 
              recipe.r_name= '". $joined["r_name"]."' AND 
              r_uses_i.i_id=ingredients.i_id AND 
              ingredients.i_id NOT IN 
                (SELECT i_id FROM mykitchen);");
        $ing_results = mysql_query($get_ing);   
        $total = 0;
        $i = 0;

        echo "<div class='moreIngredientsDiv' id = ". '"'. $id_name_nospace . '"'. ">";
  
        echo nl2br("\nYou Still Need:\n");
        while($ings = mysql_fetch_assoc($ing_results)){
          $total++;
          $i++;
          echo nl2br($ings['i_name']."\n");
          if($i != $total) echo ", ";
        }
        //echo $total; 

        echo '</div>';
        echo '</div>';
  }
  //echo "989";
}
  function addIngredient($ing){
        $insertquery = "INSERT INTO mykitchen (myingredients) SELECT '$ing' FROM DUAL WHERE NOT EXISTS(SELECT myingredients FROM mykitchen WHERE myingredients='$ing') AND EXISTS(SELECT i_name FROM ingredients WHERE i_name='$ing');";
        $query_success = mysql_query($insertquery);
        if(!$query_success){
          $message = 'Invalid query: ' . mysql_error() . "\n";
          $message .= 'Whole query: ' .$insertquery;
          die($message);
        }
        if (mysql_affected_rows() === 0) { //ingredient was invalid and NOT added
            return 0;
        }
        else{
          $updatequery = "UPDATE mykitchen SET i_id=(SELECT i_id FROM ingredients WHERE i_name = '$ing') WHERE myingredients='$ing';";
          $query_success = mysql_query($updatequery);
          if(!$query_success){
            $message = 'Invalid query: ' . mysql_error() . "\n";
            $message .= 'Whole query: ' .$updatequery;
            die($message);
          }
          else{
            return 1;
          }
        }   
  }

  function deleteIngredient($ing){
    $deletequery = sprintf("DELETE from mykitchen WHERE myingredients='%s'", mysql_real_escape_string($ing));
    $deletequery;
    mysql_query($deletequery);
  }

  function deleteAll(){
    $deletequery = sprintf("DELETE from mykitchen;");
    mysql_query($deletequery);
  }
      
  function printIngredients(){
      //print mykitchen ingredients
        $selectquery = sprintf("SELECT * from mykitchen;");
        $display_ingredients = mysql_query($selectquery);
        //If query invalid
        if(!$display_ingredients) {
          $message = 'Invalid query: ' . mysql_error() . "\n";
          $message .= 'Whole query: ' .$selectquery;
          die($message);
        }
        
        // Print results
        while($ingredient_array = mysql_fetch_assoc($display_ingredients)){
  ?>
          <div class="ingredients" id="myDiv">
  <?php
          echo '<li class="ing_name">' . $ingredient_array['myingredients'] . '&nbsp;</li>';
  ?>
          <input class="deleteIngDiv" type="button"/>
          </div>
   <?php
        }
    }

  function recipeMatches(){ //look for recipes that include mykitchen ingredients
      $cnt = 0;
      $matchquery = "SELECT r_name, r_url FROM recipe WHERE r_id not IN (SELECT r_id FROM r_uses_i WHERE r_uses_i.i_id not IN (SELECT i_id FROM mykitchen));";
      $matches = mysql_query($matchquery); //run query

      //If query invalid
      if(!$matches) {
        $message = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' .$matchquery;
        die($message);
      }
      // Print names/images of meals
      while($recipe_names = mysql_fetch_assoc($matches)){
    ?>
        <div class="reciperesult"> 
    <?php
          echo '<div class="recipeinfo"><li class="recipelink"><a href=" '. $recipe_names["r_url"] . ' " target="_blank" >' . $recipe_names["r_name"] . ' </a></li></div>';
    ?>
          <img class="recipeimg" src = "/alaminute/img/<?php echo $recipe_names['r_name']; ?>.jpg" width="250" height="200">
        </div>
  <?php
      }
  }
  
  function additionalMatches() // look for recipes that require a few additional ingredients
  {
	// Query to find recipes that require 3 or less additional ingredients that aren't already on matches list
    $joined_query = sprintf("SELECT * FROM 
    
                  (SELECT recipe.r_name, recipe.r_id, recipe.r_url, COUNT(r_uses_i.i_id) AS r_count FROM recipe, r_uses_i
                  WHERE recipe.r_id = r_uses_i.r_id
                  GROUP BY recipe.r_name) AS recipe_table
                  JOIN
                  (SELECT recipe.r_name, recipe.r_id, recipe.r_url, COUNT(mykitchen.i_id) AS k_count FROM recipe, r_uses_i, mykitchen
                  WHERE recipe.r_id=r_uses_i.r_id AND mykitchen.i_id = r_uses_i.i_id
                  GROUP BY recipe.r_name) AS kitchen_table
                  ON recipe_table.r_name = kitchen_table.r_name
                  
                WHERE r_count-k_count <= 3 
                AND kitchen_table.r_id IN (SELECT r_id FROM r_uses_i WHERE r_uses_i.i_id not IN (SELECT i_id FROM mykitchen));");
    
    $joined_results = mysql_query($joined_query);     

    //If query invalid
    if(!$joined_results) {
				$message = 'Invalid query: ' . mysql_error() . "\n";
				$message .= 'Whole query: ' .$joined_query;
				die($message);
    }
	
    ?> <!-- Additonal Meals Header -->
    <!--<div class="addMealsHeader"><img src = "/alaminute/img/MoreMealsTitle.png">
    <h3> (require a few additional ingredients) </h3>
    </div>-->
    <?php
    echo "<br>________________________________________________________________________<br>";
    
	// Display results of additional matches
    while($joined = mysql_fetch_assoc($joined_results)){
    ?>
            <div class="reciperesult">
    <?php
             echo '<div class="recipeinfo"><ul class="recipeHeader"><li class="recipelink"><a href=" '. $joined["r_url"] . ' " target="_blank" >' . $joined["r_name"] . ' </a></li><li class="infoButton"><input class="moreIngredientsView" type="button"/></li></ul></div>';
             $id_name_nospace = str_replace(' ', '', $joined['r_name']);
             $id_name_nospace = preg_replace('/\s+/', '', $id_name_nospace);
    ?>
             <img src = "/alaminute/img/<?php echo $joined['r_name']; ?>.jpg" width="250" height="200">
    <?php
		  
			// Get additional ingredients for each recipe 
				$get_ing = sprintf("SELECT i_name FROM ingredients, r_uses_i, recipe 
							WHERE r_uses_i.r_id=recipe.r_id AND 
							recipe.r_name= '". $joined["r_name"]."' AND 
							r_uses_i.i_id=ingredients.i_id AND 
							ingredients.i_id NOT IN 
								(SELECT i_id FROM mykitchen);");
				$ing_results = mysql_query($get_ing);   
				$total = 0;
				$i = 0;
    ?>
        <div class='moreIngredientsDiv' id ="<?php echo $id_name_nospace;?>">
    <?php
				echo nl2br("\nYou Still Need:\n");
				while($ings = mysql_fetch_assoc($ing_results)){
					$total++;
					$i++;
					echo nl2br($ings['i_name']."\n");
					if($i != $total) echo ", ";
				}
				//echo $total; 
    ?>
				</div>
				</div>
	<?php
      }
    }
    function countRecipeMatches(){
		$countquery = "SELECT COUNT(r_name) FROM recipe WHERE r_id not IN (SELECT r_id FROM r_uses_i WHERE r_uses_i.i_id not IN (SELECT i_id FROM mykitchen));";
		$recipecount = mysql_query($countquery);
		if(!$recipecount) {
			$message = 'Invalid query: ' . mysql_error() . "\n";
			$message .= 'Whole query: ' .$countquery;
			die($message);
		}
		while($num_matches = mysql_fetch_assoc($recipecount)){
			$cnt = $num_matches["COUNT(r_name)"];
		}
		echo "You can make ".$cnt." recipes";
	}
  /*function search_countRecipeMatches($name){
    $countquery = "SELECT COUNT(r_name) FROM recipe WHERE r_name LIKE '%$name%' AND r_id NOT IN 
    (SELECT r_id FROM r_uses_i WHERE r_uses_i.i_id not IN 
    (SELECT i_id FROM mykitchen))";
    $recipecount = mysql_query($countquery);
    if(!$recipecount) {
      $message = 'Invalid query: ' . mysql_error() . "\n";
      $message .= 'Whole query: ' .$countquery;
      die($message);
    }
    while($num_matches = mysql_fetch_assoc($recipecount)){
      $cnt = $num_matches["COUNT(r_name)"];
    }
    echo "You can make ".$cnt." recipes";
  }*/
?>