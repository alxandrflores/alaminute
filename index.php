<!-- A La Minute - index.html -->
<!DOCTYPE html>
<html class="homepage" lang="en">
  <head>

    <title>A La Minute</title>
    <!-- Custom styles for this template -->
    <link href="style.css" rel="stylesheet">

  </head>

  <body class="homepage">
    <div class="content-wrapper">
      <div>
        <nav>
          <ul class="ul-header">
            <li class="title-header">A La Minute</li>
            <li class="li-header2"><a class="active-header" href="index.php"> <img src = "/alaminute/img/logo.png" width="35" height="35"> </a></li>
            <li class="li-header"><a href="ingredients.php">Ingredients</a></li>
          </ul>
        </nav>
      </div>

		<h1>Welcome to A La Minute</h1>
        <ul class="ul-round">
          <li class="li-round">
          <a href="ingredients.php" class="round green">Start<span class="round">Find out what delicious meal is a minute away!</span> </a>
          </li>
        </ul>
      <div class="welcome"></div>

      <div class="credits">
        <p>Senior Project by Alex and Lavern</p>
     </div>
    </div>
   
  </body>
</html>
