CREATE DATABASE IF NOT EXISTS ALAMINUTE;
USE ALAMINUTE;

DROP TABLE IF EXISTS recipe;
CREATE TABLE recipe (
r_id	INTEGER			PRIMARY KEY AUTO_INCREMENT,
r_name	VARCHAR(30)		NOT NULL,
r_url	TEXT,
r_lvl	INTEGER,
r_time	VARCHAR(30)
);

DROP TABLE IF EXISTS ingredients;
CREATE TABLE ingredients (
i_id	INTEGER			PRIMARY KEY AUTO_INCREMENT,
i_name	VARCHAR(30)		NOT NULL
);

DROP TABLE IF EXISTS r_uses_i;
CREATE TABLE r_uses_i (
r_id		INTEGER				NOT NULL,
i_id		INTEGER				NOT NULL
);

DROP TABLE IF EXISTS mykitchen;
CREATE TABLE mykitchen (
myingredients	VARCHAR(30),
i_id			INTEGER 	
);

INSERT INTO recipe VALUES('1', 'Omelette', 'http://www.jamieoliver.com/recipes/eggs-recipes/omelette/', '1', '10 min');
INSERT INTO recipe VALUES('2', 'Fruit and Yogurt Parfait', 'http://www.vegkitchen.com/recipes/fruit-and-yogurt-parfaits/', '1', '15 min');
INSERT INTO recipe VALUES('3', 'French Toast', 'http://addapinch.com/perfect-french-toast-recipe/', '2', '25 min');
INSERT INTO recipe VALUES('4', 'Pancakes', 'http://allrecipes.com/recipe/21014/good-old-fashioned-pancakes/', '2', '20 min');
INSERT INTO recipe VALUES('5', 'Grilled Cheese', 'http://allrecipes.com/recipe/23891/grilled-cheese-sandwich/', '2', '20 min');
INSERT INTO recipe VALUES('6', 'Grilled Ham & Cheese Sandwich', 'http://www.kraftrecipes.com/recipes/grilled-ham-cheese-sandwiches-53460.aspx', '2', '15 min');
INSERT INTO recipe VALUES('7', 'Oatmeal', 'http://ambertheblack.com/how-to-make-oatmeal/', '2', '10 min');
INSERT INTO recipe VALUES('8', 'Breakfast Sandwich', 'http://allrecipes.com/recipe/229223/grandwich-breakfast-sandwiches/', '2', '30 min');
INSERT INTO recipe VALUES('9', 'Peanut Butter Banana Oatmeal', 'http://www.fivehearthome.com/2016/01/18/quick-peanut-butter-banana-oatmeal/', '2', '15 min');
INSERT INTO recipe VALUES('10', 'Tomato and Basil Omelette', 'http://www.jamieoliver.com/recipes/eggs-recipes/tomato-and-basil-omelette/', '1', '25 min');
INSERT INTO recipe VALUES('11', 'Mushroom and Spinach Omelette', 'http://foreverfit.tv/mushroom-and-spinach-omelette/', '1', '15 min');
INSERT INTO recipe VALUES('12', 'Crepe', 'http://allrecipes.com/recipe/16383/basic-crepes/', '2', '30 min');
INSERT INTO recipe VALUES('13', 'Strawberry Crepe', 'http://www.barbarabakes.com/strawberry-crepes/', '3', '40 min');
INSERT INTO recipe VALUES('14', 'Red Velvet Pancakes', 'http://www.cookingclassy.com/2012/12/red-velvet-pancakes-with-cream-cheese-glaze/', '4', '60 min');
INSERT INTO recipe VALUES('15', 'Garlic Brown Sugar Chicken', 'http://www.bigoven.com/recipe/garlic-brown-sugar-chicken/338212', '2', '40 min');
INSERT INTO recipe VALUES('16', 'Egg Sandwich', 'http://allrecipes.com/recipe/139551/egg-sandwich/', '1', '5 min');
INSERT INTO recipe VALUES('17', 'Pineapple Chicken Teriyaki', 'http://steamykitchen.com/24191-pineapple-chicken-teriyaki-recipe-video.html', '3', '30 min');
INSERT INTO recipe VALUES('18', 'Honey Glazed Chicken', 'http://allrecipes.com/recipe/231939/honey-glazed-chicken/', '3', '20 min');
INSERT INTO recipe VALUES('19', 'Acai Bowl', 'https://www.kaylaitsines.com/blogs/news/37028483-acai-bowl-recipe', '1', '15 min');
INSERT INTO recipe VALUES('20', 'Banana Pancakes', 'https://michigan.spoonuniversity.com/recipe/make-perfect-pancakes-2-ingredients/', '1', '15 min');

INSERT INTO ingredients VALUES('1', 'Eggs');
INSERT INTO ingredients VALUES('2', 'Cheese');
INSERT INTO ingredients VALUES('3', 'Mushrooms');
INSERT INTO ingredients VALUES('4', 'Spinach');
INSERT INTO ingredients VALUES('5', 'Ham');
INSERT INTO ingredients VALUES('6', 'Tomatoes');
INSERT INTO ingredients VALUES('7', 'Strawberries');
INSERT INTO ingredients VALUES('8', 'Blueberries');
INSERT INTO ingredients VALUES('9', 'Bananas');
INSERT INTO ingredients VALUES('10', 'Yogurt');
INSERT INTO ingredients VALUES('11', 'Granola');
INSERT INTO ingredients VALUES('12', 'Bread');
INSERT INTO ingredients VALUES('13', 'Vanilla Extract');
INSERT INTO ingredients VALUES('14', 'Cinnamon');
INSERT INTO ingredients VALUES('15', 'Milk');
INSERT INTO ingredients VALUES('16', 'Flour');
INSERT INTO ingredients VALUES('17', 'Baking Powder');
INSERT INTO ingredients VALUES('18', 'Salt');
INSERT INTO ingredients VALUES('19', 'Sugar');
INSERT INTO ingredients VALUES('20', 'Butter');
INSERT INTO ingredients VALUES('21', 'Oats');
INSERT INTO ingredients VALUES('22', 'Bacon');
INSERT INTO ingredients VALUES('23', 'Biscuits');
INSERT INTO ingredients VALUES('24', 'Peanut Butter');
INSERT INTO ingredients VALUES('25', 'Peanuts');
INSERT INTO ingredients VALUES('26', 'Honey');
INSERT INTO ingredients VALUES('27', 'Pepper');
INSERT INTO ingredients VALUES('28', 'Basil');
INSERT INTO ingredients VALUES('29', 'Cherry Tomatoes');
INSERT INTO ingredients VALUES('30', 'Olive Oil');
INSERT INTO ingredients VALUES('31', 'Cream Cheese');
INSERT INTO ingredients VALUES('32', 'Cocoa Powder');
INSERT INTO ingredients VALUES('33', 'Baking Soda');
INSERT INTO ingredients VALUES('34', 'White Vinegar');
INSERT INTO ingredients VALUES('35', 'Red Food Coloring');
INSERT INTO ingredients VALUES('36', 'Powdered Sugar');
INSERT INTO ingredients VALUES('37', 'Chicken Breast');
INSERT INTO ingredients VALUES('38', 'Brown Sugar');
INSERT INTO ingredients VALUES('39', 'Garlic');
INSERT INTO ingredients VALUES('40', 'Pineapple');
INSERT INTO ingredients VALUES('41', 'Soy sauce');
INSERT INTO ingredients VALUES('42', 'Ginger');
INSERT INTO ingredients VALUES('43', 'Red Pepper Flakes');
INSERT INTO ingredients VALUES('44', 'Acai Powder');
INSERT INTO ingredients VALUES('45', 'Raspberries');
INSERT INTO ingredients VALUES('46', 'Almonds');
INSERT INTO ingredients VALUES('47', 'Chia Seeds');

INSERT INTO r_uses_i VALUES('1', '1');
INSERT INTO r_uses_i VALUES('1', '2');
INSERT INTO r_uses_i VALUES('1', '27');
INSERT INTO r_uses_i VALUES('1', '20');
INSERT INTO r_uses_i VALUES('1', '18');

INSERT INTO r_uses_i VALUES('2', '7');
INSERT INTO r_uses_i VALUES('2', '8');
INSERT INTO r_uses_i VALUES('2', '10');
INSERT INTO r_uses_i VALUES('2', '11');

INSERT INTO r_uses_i VALUES('3', '1');
INSERT INTO r_uses_i VALUES('3', '12');
INSERT INTO r_uses_i VALUES('3', '13');
INSERT INTO r_uses_i VALUES('3', '14');
INSERT INTO r_uses_i VALUES('3', '15');
INSERT INTO r_uses_i VALUES('3', '18');
INSERT INTO r_uses_i VALUES('3', '19');
INSERT INTO r_uses_i VALUES('3', '20');

INSERT INTO r_uses_i VALUES('4', '16');
INSERT INTO r_uses_i VALUES('4', '17');
INSERT INTO r_uses_i VALUES('4', '18');
INSERT INTO r_uses_i VALUES('4', '19');
INSERT INTO r_uses_i VALUES('4', '20');
INSERT INTO r_uses_i VALUES('4', '15');
INSERT INTO r_uses_i VALUES('4', '1');

INSERT INTO r_uses_i VALUES('5', '12');
INSERT INTO r_uses_i VALUES('5', '2');
INSERT INTO r_uses_i VALUES('5', '20');

INSERT INTO r_uses_i VALUES('6', '5');
INSERT INTO r_uses_i VALUES('6', '2');
INSERT INTO r_uses_i VALUES('6', '12');
INSERT INTO r_uses_i VALUES('6', '20');

INSERT INTO r_uses_i VALUES('7', '21');
INSERT INTO r_uses_i VALUES('7', '15');
INSERT INTO r_uses_i VALUES('7', '18');
INSERT INTO r_uses_i VALUES('7', '26');

INSERT INTO r_uses_i VALUES('8', '22');
INSERT INTO r_uses_i VALUES('8', '23');
INSERT INTO r_uses_i VALUES('8', '1');
INSERT INTO r_uses_i VALUES('8', '2');

INSERT INTO r_uses_i VALUES('9', '15');
INSERT INTO r_uses_i VALUES('9', '21');
INSERT INTO r_uses_i VALUES('9', '9');
INSERT INTO r_uses_i VALUES('9', '24');
INSERT INTO r_uses_i VALUES('9', '14');
INSERT INTO r_uses_i VALUES('9', '13');
INSERT INTO r_uses_i VALUES('9', '18');
INSERT INTO r_uses_i VALUES('9', '25');
INSERT INTO r_uses_i VALUES('9', '26');

INSERT INTO r_uses_i VALUES('10', '27');
INSERT INTO r_uses_i VALUES('10', '28');
INSERT INTO r_uses_i VALUES('10', '29');
INSERT INTO r_uses_i VALUES('10', '18');
INSERT INTO r_uses_i VALUES('10', '1');

INSERT INTO r_uses_i VALUES('11', '1');
INSERT INTO r_uses_i VALUES('11', '30');
INSERT INTO r_uses_i VALUES('11', '3');
INSERT INTO r_uses_i VALUES('11', '4');
INSERT INTO r_uses_i VALUES('11', '18');
INSERT INTO r_uses_i VALUES('11', '27');

INSERT INTO r_uses_i VALUES('12', '1');
INSERT INTO r_uses_i VALUES('12', '16');
INSERT INTO r_uses_i VALUES('12', '15');
INSERT INTO r_uses_i VALUES('12', '18');
INSERT INTO r_uses_i VALUES('12', '20');

INSERT INTO r_uses_i VALUES('13', '7');
INSERT INTO r_uses_i VALUES('13', '19');
INSERT INTO r_uses_i VALUES('13', '1');
INSERT INTO r_uses_i VALUES('13', '20');
INSERT INTO r_uses_i VALUES('13', '15');
INSERT INTO r_uses_i VALUES('13', '13');
INSERT INTO r_uses_i VALUES('13', '18');
INSERT INTO r_uses_i VALUES('13', '16');

INSERT INTO r_uses_i VALUES('14', '31');
INSERT INTO r_uses_i VALUES('14', '32');
INSERT INTO r_uses_i VALUES('14', '33');
INSERT INTO r_uses_i VALUES('14', '34');
INSERT INTO r_uses_i VALUES('14', '35');
INSERT INTO r_uses_i VALUES('14', '36');
INSERT INTO r_uses_i VALUES('14', '15');
INSERT INTO r_uses_i VALUES('14', '16');
INSERT INTO r_uses_i VALUES('14', '17');
INSERT INTO r_uses_i VALUES('14', '18');
INSERT INTO r_uses_i VALUES('14', '19');
INSERT INTO r_uses_i VALUES('14', '20');
INSERT INTO r_uses_i VALUES('14', '13');
INSERT INTO r_uses_i VALUES('14', '1');

INSERT INTO r_uses_i VALUES('15', '37');
INSERT INTO r_uses_i VALUES('15', '38');
INSERT INTO r_uses_i VALUES('15', '39');
INSERT INTO r_uses_i VALUES('15', '30');

INSERT INTO r_uses_i VALUES('16', '1');
INSERT INTO r_uses_i VALUES('16', '2');
INSERT INTO r_uses_i VALUES('16', '12');
INSERT INTO r_uses_i VALUES('16', '15');

INSERT INTO r_uses_i VALUES('17', '26');
INSERT INTO r_uses_i VALUES('17', '37');
INSERT INTO r_uses_i VALUES('17', '39');
INSERT INTO r_uses_i VALUES('17', '40');
INSERT INTO r_uses_i VALUES('17', '41');
INSERT INTO r_uses_i VALUES('17', '42');

INSERT INTO r_uses_i VALUES('18', '26');
INSERT INTO r_uses_i VALUES('18', '30');
INSERT INTO r_uses_i VALUES('18', '37');
INSERT INTO r_uses_i VALUES('18', '41');
INSERT INTO r_uses_i VALUES('18', '43');

INSERT INTO r_uses_i VALUES('19', '4');
INSERT INTO r_uses_i VALUES('19', '7');
INSERT INTO r_uses_i VALUES('19', '8');
INSERT INTO r_uses_i VALUES('19', '9');
INSERT INTO r_uses_i VALUES('19', '15');
INSERT INTO r_uses_i VALUES('19', '26');
INSERT INTO r_uses_i VALUES('19', '44');
INSERT INTO r_uses_i VALUES('19', '45');
INSERT INTO r_uses_i VALUES('19', '46');
INSERT INTO r_uses_i VALUES('19', '47');

INSERT INTO r_uses_i VALUES('20', '1');
INSERT INTO r_uses_i VALUES('20', '9');
























































































