<!-- A La Minute - ingredients.html -->
<!DOCTYPE html>
<html lang="en">

  <head>

    <title>A La Minute</title>

    <!-- Custom styles for this template -->
    <link href="style.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

  </head>
  <body onload="displayCount()">
    <div class="content-wrapper">
      <div>
        <nav>
          <ul class="ul-header">
            <li class="title-header">A La Minute</li>
            <li class="li-header2"><a href="index.php"> <img src = "/alaminute/img/logo.png" width="35" height="35"></a></li>
            <li class="li-header"><a class="active-header" href="ingredients.php">Ingredients</a></li>
          </ul>
        </nav>
      </div>
      <script>
          function displayCount(){
            callPage('countRecipeMatches.php',"recipecount");
            
          }
        </script>
        <!-- <script>
          function search_displayCount(search_text){
            callPage('search_countRecipeMatches.php?search_recipe='+search_text.trim(),"recipecount");
          }
        </script> -->

        <script>
          $(document).ready(function(){
            $(".moreIngredientsView").click(function(){
              var recipeName = $(this).parent().parent().text();
              var recipeName_nospace = recipeName.replace(/\s/g, '');
              //var additionalIng = $(this).children().text();
              var additionalIng = $("#"+recipeName_nospace).text();
              alert(additionalIng);
            });
          });
        </script>
       <!-- <script>
          $(document).ready(function(){
              $(".searchbutton").click(function(){
                var search_name = "oat";
                search_displayCount(search_name);
              });
            });
        </script> -->
        
        <div id='Results' class='matches'>
          <?php

            require 'connect.php';
            include 'functions.php';

            if(isset($_POST['addbutton'])){
              
              $ingredient = $_POST['addingredient'];
              if(!empty($ingredient)){
                $result = addIngredient($ingredient);
                if($result === 0){
                 //echo "<div class='invalidAdd'>'".$ingredient . "' could not be added</div>";
                }
                recipeMatches();
                additionalMatches();
              }
              unset($_POST['addbutton']);
              unset($_POST['addingredient']);
            }
            if(isset($_GET['ingredient_name_delete'])){
              recipeMatches();
              additionalMatches();
            }
            if(isset($_POST['deleteAll'])){
                deleteAll();
            }
            if(isset($_POST['searchbutton'])){
              $recipe = $_POST['searchbox'];
              searchRecipe($recipe);
              //search_countRecipeMatches($recipe);
            }
            if(isset($_POST['clearfilterbutton'])){
              recipeMatches();
              additionalMatches();
            }
            /*else{
              recipeMatches();
              additionalMatches();
            }*/
          ?>

        </div>
        <div class="RecipeCountHeader"> 
          <ul class="recipe-ul">
            <li class="infoHeaderImg"><img src="/alaminute/img/info.png" width="30" height="30"></li>
            <li class="infoHeader">Missing Ingredients</li>
            <li class="recipecounter" id="recipecount"></li>
            </ul>
        </div>
        <div class='mykitchen'>
          <form action="ingredients.php" method="POST">
            <input class="searchbox" id="searchRecipe" type="text" placeholder="Search Recipe" name="searchbox"/> 
            <input class="searchbutton" type="submit" value="" name = "searchbutton">
          </form>
          <!-- 
          <select>
                <option value="something">something</option>
                <option value="something_else">something else</option>
          </select>
        -->
          <form action="ingredients.php" method="POST">
            <input class="clearFilterButton"type="submit" value="Clear Search" name = "clearfilterbutton">
          </form>
          <hr>

          <form action="ingredients.php" method="POST">
             <input class="addbox" id="addIng"  type="text" placeholder="Add Ingredients" name="addingredient"/> <!-- input for MyKitchen -->
             <input class="addbutton" type="submit" value="" name = "addbutton"><br><br><br>

             <!--<input class="removebox" id="deleteIng" type="text" placeholder="Remove Ingredients" name="delingredient"/> 
             <input type="submit" value="Remove" name = "deletebutton">-->
          </form>

          <h2 id="demo">My Kitchen</h2><br>
          <?php
           printIngredients(); //display ingredients user has added
          ?>
    		  <form action="ingredients.php" method="POST">
    		    <br><input class="clearingbutton" type="submit" value="Clear Ingredients" name="deleteAll">
    		  </form>

        </div>

          <!-- Script function to remove ingredient <div> when clicked -->
          <script type="text/javascript">
            function callPage(url,div){
              var xhttp = new XMLHttpRequest();
              xhttp.onreadystatechange = function() {
              if (xhttp.readyState == 4 && xhttp.status == 200) {
                document.getElementById(div).innerHTML = xhttp.responseText;
                //alert(xhttp.responseText);
              }
              };
              xhttp.open("GET", url, true);
              xhttp.send();
            }

            $(document).ready(function() {
              $(".deleteIngDiv").click(function () {
                $(this).parent().remove();
                var str = $(this).parent().text(); //store ingredient name in string
                
                callPage('deleteingredients.php?ingredient_name_delete='+str.trim(),"Results");
                callPage('countRecipeMatches.php',"recipecount");
                location.reload();
              });
            });
          </script>
          <script>
            $( document ).ready(function() {
                $(".invalidAdd").delay(3000).fadeOut("slow");
            });
          </script>
          <!--
          <script>
            $(document).ready(function(){
                $(".clearfilterbutton, .addbutton, .deleteIngDiv").click(function(){
                  alert("4");
                  displayCount();
                });
              });
        </script>
      -->
    </div>
  </body>
</html>
